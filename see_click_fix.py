#!/usr/bin/env python3

import urllib
import urllib.request
import bs4
import datetime


class Issue:

    def __init__(self, identifier, reporter, category, view_count, reported_date, comments):
        self.identifier = identifier
        self.reporter = reporter
        self.category = category
        self.view_count = view_count
        self.reported_date = reported_date
        self.comments = comments

    def __repr__(self):
        return f'<Issue identifier {self.identifier} author {self.reporter}, ' \
               f'category \'{self.category}\', view_count: {self.view_count}, reported_date: {self.reported_date}, comments: {self.comments}>'


class Author:

    def __init__(self, name, identifier, is_verified_official):
        self.name = name
        self.identifier = identifier
        self.is_verified_official = is_verified_official

    def __repr__(self):
        return f'<Author name {self.name}, identifier {self.identifier}, verified: {self.is_verified_official}'


class Comment:

    def __init__(self, identifier, author, message_text, is_acknowledged, is_closed, date):
        self.identifier = identifier
        self.author = author
        self.message_text = message_text
        self.is_acknowledged = is_acknowledged
        self.is_closed = is_closed
        self.date = date

    def __repr__(self):
        return f'<Comment identifier {self.identifier} author {self.author}, ' \
               f'message text \'{self.message_text}\', acknowledged: \'{self.is_acknowledged}\', closed: \'{self.is_closed}\', date: \'{self.date}\'>'


def comment_from_tag(comment_tag):
    def is_name_tag(tag):
        return tag.name == 'a' and tag.has_attr('name')
    comment_id = comment_tag.find(is_name_tag)['name'].strip().split('-')[1]
    is_acknowledged = comment_tag.find('span', class_='acknowledged') is not None
    is_closed = comment_tag.find('span', class_='closed') is not None
    comment_message = ''
    comment_message_tag = comment_tag.find('div', itemprop='commentText')
    if comment_message_tag:
        comment_message = comment_message_tag.text.strip()
    is_verified_official = comment_tag.find(class_='fwn', string='(Verified Official)') is not None
    author_name = comment_tag.find('a', itemprop='creator').text.strip()
    author_id = comment_tag.find('a', itemprop='creator')['href'].strip().split('/')[2]
    date_time_string = comment_tag.find('time')['datetime'].strip()
    date_time = datetime.datetime.strptime(date_time_string, '%Y-%m-%d %H:%M:%S')
    author = Author(author_name, author_id, is_verified_official)
    comment = Comment(comment_id, author, comment_message, is_acknowledged, is_closed, date_time)
    return comment


def parse_issue_page_soup(issue_page_soup):
    try:
        comment_tags = issue_page_soup.find_all('li', class_='comment')
        comments = [comment_from_tag(tag) for tag in comment_tags]

        # Issue ID
        issue_id_label_tag = issue_page_soup.find('h4', class_='inline', string='Issue ID:')
        issue_id_div_tag = issue_id_label_tag.parent
        issue_id = issue_id_div_tag.contents[2].strip()
        reporter = None

        # Category
        category_label_tag = issue_page_soup.find('h4', class_='inline', string='Category:')
        category_div_tag = category_label_tag.parent
        category = category_div_tag.contents[2].strip()

        # View Count
        view_count_label_tag = issue_page_soup.find('h4', class_='inline', string='Viewed:')
        view_count_div_tag = view_count_label_tag.parent
        view_count = int(view_count_div_tag.contents[2].strip().split(' ')[0].strip())

        # Reported Date
        reported_date_string_label_tag = issue_page_soup.find('h4', class_='inline', string='Reported:')
        reported_date_string_div_tag = reported_date_string_label_tag.parent
        reported_date_string = reported_date_string_div_tag.contents[3]['datetime'].strip()
        reported_date = datetime.datetime.strptime(reported_date_string, '%Y-%m-%d %H:%M:%S')
        issue = Issue(issue_id, reporter, category, view_count, reported_date, comments)
        return issue
    except Exception as e:
        print(f'Problem parsing issue page!')
        raise e


def parse_url(url_string):
    try:
        page_html = urllib.request.urlopen(url_string)
        page_soup = bs4.BeautifulSoup(page_html, 'html.parser')
        issue = parse_issue_page_soup(page_soup)
        print(issue)
    except Exception as e:
        print(f'Problem parsing page at URL {url_string}!')
        raise e


if __name__ == "__main__":
    parse_url('https://seeclickfix.com/issues/3741669')
